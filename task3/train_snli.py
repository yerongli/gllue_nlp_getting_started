import os
import argparse
import pickle
import torch
import json

import matplotlib.pyplot as plt
import torch.nn as nn

from torch.utils.data import DataLoader

def main(train_file,
         valid_file,
         embeddings_file,
         target_dir,
         hidden_size=20,
         dropout=0.5,
         num_classes=3,
         epochs=1,   # EDIT
         batch_size=32,
         lr=0.0004,
         patience=5,
         max_grad_norm=10.0,
         checkpoint=None):
    pass

if __name__ == "__main__":
    '''
    default_config = "../../config/training/snli_training.json"

    parser = argparse.ArgumentParser(description="Train the ESIM model on SNLI")
    parser.add_argument("--config",
                        default=default_config,
                        help="Path to a json configuration file")
    parser.add_argument("--checkpoint",
                        default=None,
                        help="Path to a checkpoint file to resume training")
    args = parser.parse_args()

    script_dir = os.path.dirname(os.path.realpath(__file__))

    if args.config == default_config:
        config_path = os.path.join(script_dir, args.config)
    else:
        config_path = args.config

    with open(os.path.normpath(config_path), 'r') as config_file:
        config = json.load(config_file)
    '''
    script_dir = os.path.dirname(os.path.realpath(__file__))
    print(script_dir)
    config = {
    "train_data": "../../data/preprocessed/SNLI/train_data.pkl",
    "valid_data": "../../data/preprocessed/SNLI/dev_data.pkl",
    "embeddings": "../../data/preprocessed/SNLI/embeddings.pkl",

    "target_dir": "../../data/checkpoints/SNLI",

    "hidden_size": 300,
    "dropout": 0.5,
    "num_classes": 3,

    "epochs": 64,
    "batch_size": 32,
    "lr": 0.0004,
    "patience": 5,
    "max_gradient_norm": 10.0
    }
    main(os.path.normpath(os.path.join(script_dir, config["train_data"])),
         os.path.normpath(os.path.join(script_dir, config["valid_data"])),
         os.path.normpath(os.path.join(script_dir, config["embeddings"])),
         os.path.normpath(os.path.join(script_dir, config["target_dir"])),
         config["hidden_size"],
         config["dropout"],
         config["num_classes"],
         config["epochs"],
         config["batch_size"],
         config["lr"],
         config["patience"],
         config["max_gradient_norm"],
         args.checkpoint)