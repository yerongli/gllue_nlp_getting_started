# GLLUE NLP 从入门到放弃培训文档
本文参考 复旦NLP实验室入门指导

相关资源：

- [《神经网络与机器学习》](https://nndl.github.io/)

## 完成要求

加入培训的同学，从主分支切出一条分支 *mission_姓名* 用来提交相关训练结果（记录文档+试验代码）。放在个人名的文件夹下。

-----

### 任务一：基于机器学习的文本分类

- 任务目标：实现基于传统机器学习算法（logistic/softmax regression 、naive bayes、svm 等）的文本分类任务

- 数据集：[Sentiment Analysis on Movie Reviews](https://www.kaggle.com/c/sentiment-analysis-on-movie-reviews)

- 参考： [《神经网络与机器学习》](https://nndl.github.io/) 第2、3章

- 预计时间: 1天

- 任务重点:
  - 文本特征表示：Bag-of-Word，N-gram
  - 特征选择
  - 损失函数
  - 分类器：logistic/softmax regression、naive bayes、svm 等
  - 数据集：训练集/验证集/测试集的划分

### 任务二：基于深度学习的文本分类

- 任务目标：实现基于深度学习（CNN、RNN）的文本分类
- 工具：Keras、TensorFlow、Pytorch、glove、gensim
- 数据集：参考任务一
- 参考：
  - [《神经网络与机器学习》](https://nndl.github.io/) 第5、6、7章
  - [Convolutional Neural Networks for Sentence Classification](https://arxiv.org/abs/1408.5882)
  - [Sequence Classification with LSTM Recurrent Neural Networks in Python with Keras](https://machinelearningmastery.com/sequence-classification-lstm-recurrent-neural-networks-python-keras/)
  - [Keras Documentation](https://keras.io/)
  - [PyTorch中文文档](https://pytorch-cn.readthedocs.io/zh/latest/)

- 任务重点：
  - Word2vec
  - WordEmbedding
  - CNN/RNN的特征抽取
  - Dropout
  - BatchNormalization

- 时间：1天

### 任务三：实现基于注意力机制的文本匹配

- 任务目标：实现文本匹配算法，输入两个句子、判断他们之间的关系。尝试使用双向的注意力机制实现。

- 数据集：[SNLI](https://nlp.stanford.edu/projects/snli/)
- 参考：
  - [《神经网络与机器学习》](https://nndl.github.io/) 第8章
  - [Enhanced LSTM for Natural Language Inference](https://arxiv.org/pdf/1609.06038v3.pdf)
  - [Reasoning about Entailment with Neural Attention](https://arxiv.org/pdf/1509.06664v1.pdf)

- 任务重点:
  - 注意力机制
  - token2token attetnion
- 时间：1天
  
### 任务四：深度学习的序列标注

- 任务目标：用LSTM+CRF来训练序列标注模型，以Named Entity Recognition为例。也可尝试其他方式，如膨胀卷积。
- 数据集：[CONLL 2003](https://www.clips.uantwerpen.be/conll2003/ner/)
- 参考：
  - [《神经网络与机器学习》](https://nndl.github.io/) 第6，11章
  - [End-to-end Sequence Labeling via Bi-directional LSTM-CNNs-CRF](https://arxiv.org/pdf/1603.01354.pdf)
  - [Neural Architectures for Named Entity Recognition](https://arxiv.org/pdf/1603.01360.pdf)

- 任务重点：
  - 评价指标：precision、recall、F1
  - 无向图模型、CRF

- 时间: 1天

### 任务五：基于神经网络的语言模型

- 任务目标：用LSTM、GRU来训练字符级的语言模型，计算困惑度
- 数据集：[唐诗]('./data/tang_poetry.txt')、可尝试其他生成数据集
- 参考：[《神经网络与机器学习》](https://nndl.github.io/) 第6，15章
- 任务重点：
  - 语言模型
  - 困惑度指标
  - 文本生成
- 时间：1天

### 拓展：

- BERT
- GPT-2