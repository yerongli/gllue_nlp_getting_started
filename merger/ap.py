# -*- coding: utf-8 -*-
"""
Created on Wed Nov 30 10:05:42 2016

@author: Administrator
"""
from unionfind import UnionFind
from numpy import *
import itertools, gc, json, os, operator, re, timeit, pymongo
from loc_recognizer import LocTrie
from elasticsearch import Elasticsearch


# 生成原始数据，用于测试
def loadDataSet():
    # return [[1, 3, 4], [2, 3, 5], [1, 2, 3, 5], [2, 5]]
    '''return [(['r', 'z', 'h', 'j', 'p'], 1),
            (['z', 'y', 'x', 'w', 'v', 'u', 't', 's'], 2),
            (['z'], 1),
            (['r', 'x', 'n', 'o', 's'], 1),
            (['y', 'r', 'x', 'z', 'q', 't', 'p'], 1),
            (['y', 'z', 'x', 'e', 'q', 's', 't', 'm'], 1)]'''

    try:
        file = os.path.join(os.environ["HOME"], "data", "company_count.json")
        with open(file) as json_file:
            data_ = json.load(json_file)
    except:
        file = os.path.join(os.environ["HOME"],'Downloads' ,"data" ,"company_count.json")
        with open(file) as json_file:
            data_ = json.load(json_file)

    sorted_x = sorted(data_.items(), key=operator.itemgetter(1), reverse = True)[:100000]
    data = []
    # cnt = 0
    checker = LocTrie().is_loc
    '''
    data_ = {
        '腾讯科技(深圳)有限公司': 11952,
        '软通动力信息技术(集团)有限公司': 12847,
        '软通动力信息技术(集团)': 12847,
        '宝洁公司（PROCTER & GAMBLE）': 2816,
        '阿里巴巴蚂蚁金服-支付宝': 2,
        '蚂蚁金服(支付宝)' : 100
    }
    '''
    re_split = re.compile('(?<=[\u4e00-\u9fa5,0-9])-(?=[\u4e00-\u9fa50-9A-Z])')
    m = {}
    for o_line, _ in sorted_x:
        line = o_line.replace('（', '(')
        line = line.replace('）', ')')
        line = line.replace('，', ',')
        pars = re.findall('\(([^)]+)', line)
        for p in pars:
            if '特殊普通合伙' == p:
                continue
            # print(p, 'p')
            line = line.replace('(' + p + ')', '')
            if not (checker(p) or len(p) < 2 or re.match(r'^集团', p) or 'China' == p or '亚洲' == p or '国际' == p):
                line = line + '-' + p
        lst = re_split.split(line)
        name_set = set()
        if len(lst) > 1:
            data.append((lst, data_[o_line]))
            name_set.update(lst)
                
    return data, list(name_set)

# 获取整个数据库中的一阶元素

def createC1(dataSet):
    '''
    '''
    C1 = set([])
    for item, cnt in dataSet:
        C1 = C1.union(set(item))
    return [frozenset([i]) for i in C1]


# 输入数据库（dataset） 和 由第K-1层数据融合后得到的第K层数据集（Ck），
# 用最小支持度（minSupport)对 Ck 过滤，得到第k层剩下的数据集合（Lk）
def getLk(dataset, Ck, minSupportNum, support_dic):
    Lk = {}
    # 计算Ck中每个元素在数据库中出现次数
    for item, cnt in dataset:
        for Ci in Ck:
            if Ci.issubset(item):
                if not Ci in Lk:
                    Lk[Ci] = cnt
                else:
                    Lk[Ci] += cnt
    # 用最小支持度过滤
    Lk_return = []
    for Li in Lk:
        if Lk[Li] >= minSupportNum:
            Lk_return.append(Li)
            support_dic[Li] = Lk[Li]
    return Lk_return


# 将经过支持度过滤后的第K层数据集合（Lk）融合
# 得到第k+1层原始数据Ck1


def genLk1(Lk):
    Ck1 = []
    for i in range(len(Lk) - 1):
        for j in range(i + 1, len(Lk)):
            if sorted(list(Lk[i]))[0:-1] == sorted(list(Lk[j]))[0:-1]:
                Ck1.append(Lk[i] | Lk[j])
    return Ck1


# 遍历所有二阶及以上的频繁项集合
def genItem(freqSet, support_dic):
    for i in range(1, len(freqSet)):
        for freItem in freqSet[i]:
            genRule(freItem)


# 输入一个频繁项，根据“置信度”生成规则
# 采用了递归，对规则树进行剪枝
def genRule(Item, minConf=0.7):
    if len(Item) >= 2:
        for element in itertools.combinations(list(Item), 1):
            if support_dic[Item] / float(support_dic[Item - frozenset(element)]) >= minConf:
                print(str([Item - frozenset(element)]) + "----->" + str(element))
                print(support_dic[Item] / float(support_dic[Item - frozenset(element)]))
                genRule(Item - frozenset(element))

def apriori(dataset, min_support_num = 10):
    support_dic = {}
    Ck = createC1(dataSet)
    for k in range(2):
        # support_dic.clear()
        # while True:
        Lk = getLk(dataSet, Ck, min_support_num, support_dic)
        del Ck
        gc.collect()
        if not Lk:
            break
        result_list.append(Lk)
        Ck = genLk1(Lk)
        if not Ck:
            break
        del Lk
        gc.collect()
    # 输出频繁项及其“支持度”
    return support_dic, result_list
                
# 输出结果
if __name__ == '__main__':
    dataSet, name_list = loadDataSet()
    gc.collect()
    result_list = []
    start = timeit.default_timer()
    support_dict, result_list = apriori(dataSet, )
    uf = UnionFind(name_list)
    for edge in support_dict:
        if len(edge) > 1:
            tmp = list(edge)
            uf.union(tmp[0], tmp[1])
    dd = uf.component_mapping()
    myclient = pymongo.MongoClient("mongodb://10.0.0.21:27017/")
    mydb = myclient["ai_search_engine"]
    mycol = mydb["company_aliases"]

    dictlist = []
    for key, value in dd.items():
        if len(value) > 1:
            #print([frozenset(value)], '[frozenset(value)]')
            try:
                temp = {'name': key, 'candidates': [{'name' : v, 'score' : float(support_dict[frozenset({v, key})])/float(support_dict[frozenset([v])])} for v in value]}
            except:
                temp = {'name': key, 'candidates': [{'name' : v, 'score' : 0.0} for v in value]} 
            dictlist.append(temp)
            #client.index(index = 'company_aliases', doc_type='company', body = temp)
    
    print(dictlist)
    mycol.insert_many(dictlist)  
    print(timeit.default_timer() - start)
# 输出规则
# genItem(result_list, support_dic)
