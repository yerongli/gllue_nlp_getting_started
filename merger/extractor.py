import tqdm
from pymongo import MongoClient
from collections import defaultdict

client = MongoClient('10.0.0.21')
db = client.resume
col = db.resume

school_dc = defaultdict(int)
major_dc = defaultdict(int)
degree_dc = defaultdict(int)

company_dc = defaultdict(int)
title_dc = defaultdict(int)
industry_dc = defaultdict(int)

email_dc = defaultdict(int)
name_dc = defaultdict(int)
mobile_dc = defaultdict(int)

data = col.find()

for r in tqdm.tqdm(data, total=data.count()):
    name = r.get('name') or r.get("chineseName")
    mobile = r.get('mobile')
    email = r.get('email')

    if name:
        name_dc[name] += 1

    if mobile:
        mobile_dc[mobile] += 1

    if email:
        email_dc[email] += 1

    educations = r.get('educations')
    experiences = r.get('experiences')

    if educations:
        for edu in educations:
            school = edu.get('school')
            major = edu.get('major')
            degree = edu.get('degree')

            if school:
                school_dc[school] += 1

            if major:
                major_dc[major] += 1

            if degree:
                degree_dc[degree] += 1

    if experiences:
        for exp in experiences:
            company = exp.get('company') or exp.get('name')
            title = exp.get('title')
            industry = exp.get('industry')

            if company:
                company_dc[company] += 1

            if title:
                title_dc[title] += 1

            if industry:
                industry_dc[industry] += 1