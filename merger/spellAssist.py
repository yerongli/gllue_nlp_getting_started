import os, operator, json, timeit

from symspellpy.symspellpy import SymSpell, Verbosity  # import the module
class SpellAssist:
    def __init__(self, max_edit_distance_dictionary = 2, prefix_length = 7):
        # maximum edit distance per dictionary pre-calculation
        self.max_edit_distance_dictionary = max_edit_distance_dictionary
        self.prefix_length = prefix_length
        
        
        self.company_spell = SymSpell(self, max_edit_distance_dictionary, prefix_length)


def main():

    
    # load dictionary
    try:
        with open("/tmp/company_count.json") as json_file:
            data_ = json.load(json_file)
    except FileNotFoundError:
        file = os.path.join(os.environ["HOME"], "data", "company_count.json")
        with open(file) as json_file:
            data_ = json.load(json_file)
    sorted_x = sorted(data_.items(), key=operator.itemgetter(1), reverse=True)[:200000]
    for k, v in sorted_x:
        sym_spell.create_dictionary_entry(k, v)
    start = timeit.default_timer()
    # lookup suggestions for single-word input strings
    input_term = "百度a"  # misspelling of "members"
    # max edit distance per lookup
    # (max_edit_distance_lookup <= max_edit_distance_dictionary)
    max_edit_distance_lookup = 2
    suggestion_verbosity = Verbosity.CLOSEST  # TOP, CLOSEST, ALL
    suggestions = sym_spell.lookup(input_term, suggestion_verbosity,
                                   max_edit_distance_lookup)

    # display suggestion term, term frequency, and edit distance
    for suggestion in suggestions:
        print("{}, {}, {}".format(suggestion.term, suggestion.distance,
                                  suggestion.count))
    print('Total Merging time:', timeit.default_timer() - start, 'seconds')


if __name__ == "__main__":
    main()
