# -*- coding: utf-8 -*-
import json, os, operator, timeit, re, baidu_crawler, reg
from queue import PriorityQueue
from difflib import SequenceMatcher
from heapq import nlargest as _nlargest
similarity_cutoff = 1.0/8.0
number_neighbours = 5
url_length = 8
'''
    Merger based on the baidu crawler
'''

'''
    Match class
        Match class is a string with a matching score,
        the matching score is intersection with baidu/bing
        crawler
'''
class Match:
    def __init__(self, id_, score):
        self.id = id_
        self.score = score
    def __lt__(self, obj):
        return self.score > obj.score



def intersection_ratio(lst1, lst2):
    '''
    Get number of duplicated items in two lists
    :param lst1: list 1
    :param lst2: list 2ß
    :return: number of duplicated items in list 1 and list 2
    '''
    assert(len(lst1) == len(lst2))
    lst3 = [value for value in lst1 if value in lst2]
    return float(len(lst3))/float(len(lst1))

def get_matches(word, possibilities, n=3, cutoff=0.6):
    """Use SequenceMatcher to return list of the best "good enough" matches.

    word is a sequence for which close matches are desired (typically a
    string).

    possibilities is a list of sequences against which to match word
    (typically a list of strings).

    Optional arg n (default 3) is the maximum number of close matches to
    return.  n must be > 0.

    Optional arg cutoff (default 0.6) is a float in [0, 1].  Possibilities
    that don't score at least that similar to word are ignored.

    The best (no more than n) matches among the possibilities are returned
    in a list, sorted by similarity score, most similar first.

    >>> get_matches("appel", ["ape", "apple", "peach", "puppy"])
    ['apple', 'ape']
    >>> import keyword as _keyword
    >>> get_matches("wheel", _keyword.kwlist)
    ['while']
    >>> get_matches("Apple", _keyword.kwlist)
    []
    >>> get_matches("accept", _keyword.kwlist)
    ['except']
    """

    if not n >  0:
        raise ValueError("n must be > 0: %r" % (n,))
    if not 0.0 <= cutoff <= 1.0:
        raise ValueError("cutoff must be in [0.0, 1.0]: %r" % (cutoff,))
    result = []
    s = SequenceMatcher()
    s.set_seq2(word)
    for i, x in enumerate(possibilities):
        s.set_seq1(x)
        if s.real_quick_ratio() >= cutoff and \
           s.quick_ratio() >= cutoff and \
           s.ratio() >= cutoff:
            result.append((s.ratio(), i))

    # Move the best scorers to head of list
    result = _nlargest(n, result)
    # Strip scores for the best n matches
    print(result)
    return [i for score, i in result]
def _generate_adj(company_id, adj, lst, filtered, default_degree = 4):
    '''
    Generate/Pad 
    :param company_id: The input company id, should be an integer
    :param adj: The adjacency matix/dictionary
    :param lst: List of companies ordered by occurrence counts
    :param default_degree: default maximum number of connections
        each company can take
    :return: None
    '''
    sz = len(adj[company_id]) if company_id in adj else 0
    degree = 0 if sz >= default_degree else default_degree - sz
    if 0 == degree:
        return
    match_ids = get_matches(filtered[company_id], filtered[:1000], n = 1 + number_neighbours, cutoff = 0.4)
    #match_ids.remove(company_id)
    try:
        _crawler = baidu_crawler.crawler(lst[company_id])
        pattern_urls = _crawler.run()[: url_length]
    except:
        print(lst[company_id], end = ' ')
        print('error : Crawler intialization error')
        exit(0)
    print(operator.itemgetter(*match_ids)(lst), ' matches')
    pq = PriorityQueue()
    for match_id in match_ids:
        if match_id == company_id:
            continue
        try:
            _crawler.set_keyword(lst[match_id])
            pairing_urls = _crawler.run()[: url_length]
        except:
            print(str(match_id) + ' id')
            exit(0)
        try:
            print(intersection_ratio(pairing_urls, pattern_urls))
            pq.put(Match(match_id, intersection_ratio(pairing_urls, pattern_urls)))
        except:
            print(pairing_urls, pattern_urls)
            print('error : Intersection')
            exit(0)
    i = 0
    while i < degree and not pq.empty():
        pair = pq.get()
        if pair.score >= similarity_cutoff :
            adj[company_id].add(pair.id)
            i+= 1
        else:
            break

def generate_adj(lst, filtered, adj):
    start = timeit.default_timer()
    for id_ in range(len(lst)):
        print(id_, lst[id_])
        _generate_adj(id_, adj, lst, filtered)
        print(adj[id_])

if __name__ == '__main__':
    print('START')
    try:
        file = os.path.join(os.environ["HOME"], "data", "company_count.json")
        with open(file) as json_file:
            data = json.load(json_file)
    except:
        file = os.path.join(os.environ["HOME"],'Downloads' ,"data" ,"company_count.json")
        with open(file) as json_file:
            data = json.load(json_file)
    length = 10000 # DEBUG
    lst = sorted(data.items(), key = operator.itemgetter(1), reverse = True)[:length]
    length = len(lst)
    lst = [item[0] for item in lst]
    filtered = list(filter(None, map(lambda item: reg.reg_company.sub('', item), lst)))
    adj = {id_ : set() for id_ in range(length)}
    generate_adj(lst, filtered, adj)
