#!/usr/bin/python

import re, importlib, sys, getopt, requests
from urllib.request import quote
importlib.reload(sys)

class crawler:

    '''
        Default timeout should be 60 seconds
    '''
    timeout = 60
    '''
        Header parameters for Baidu crawler
    '''
    headersParameters = {
        'Connection': 'Keep-Alive',
        'Accept': 'text/html, application/xhtml+xml, */*',
        'Accept-Language': 'en-US,en;q=0.8,zh-Hans-CN;q=0.5,zh-Hans;q=0.3',
        'Accept-Encoding': 'gzip, deflate',
        'User-Agent': 'Mozilla/6.1 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko'
    }

    def __init__(self, keyword):
        '''
            crawler for baidu search engine result 
        '''
        self.url = u''
        self.urls = []
        self.o_urls = []
        self.html = ''
        self.total_pages = 2
        self.next_page_url = ''
        self.current_page = 0
        self.url = u'https://www.baidu.com/baidu?wd=' + quote(keyword) + '&tn=monline_dg&ie=utf-8'

    def set_timeout(self, time):
        '''timeout for '''
        try:
            self.timeout = int(time)
        except:
            pass

    def set_total_pages(self, num):
        '''Set total pages from baidu'''
        try:
            self.total_pages = int(num)
        except:
            pass

    def set_keyword(self, keyword):
        self.__init__(keyword)

    def set_current_url(self, url):
        '''Set current_url'''
        self.url = url

    def switch_url(self):
        '''
            Switch to next page url
        '''
        if self.next_page_url == '':
            sys.exit()
        else:
            self.set_current_url(self.next_page_url)

    def is_finish(self):
        '''

        '''
        if self.current_page >= self.total_pages:
            return True
        else:
            return False

    def get_html(self):
        '''爬取当前url所指页面的内容，保存到html中'''
        r = requests.get(self.url ,timeout = self.timeout, headers = self.headersParameters)
        if 200 == r.status_code:
            self.html = r.text
            self.current_page+= 1
        else:
            self.html = u''
            print('[ERROR]',self.url,u'get此url返回的http状态码不是200')

    def get_urls(self):
        '''
        Get original urls and store them into the self.orls
        :return: None
        '''
        page = 0
        while page < self.total_pages and page > -1:
            o_urls = re.findall('href\=\"(http\:\/\/www\.baidu\.com\/link\?url\=.*?)\" class\=\"c\-showurl\"', self.html)
            self.o_urls+= o_urls
            # Move on the to the next page
            _next = re.findall(' href\=\"(\/s\?wd\=[\w\d\%\&\=\_\-]*?)\" class\=\"n\"', self.html)
            if len(_next) > 0:
                self.next_page_url = 'https://www.baidu.com'+ _next[-1]
                self.switch_url()
                self.get_html()
                page+= 1
            else:
                self.next_page_url = ''
                page = -1

    
    def get_title(self, url):
        return re.findall('\<title.*?\>(.+?)title\>', requests.get(url ,timeout = self.timeout, headers = self.headersParameters).text)

    def get_real(self, o_url):
        '''
        Get real urls from directing urls
        '''
        '''
        Disable redirects
        '''
        r = requests.get(o_url, allow_redirects = False)
        if r.status_code == 302:
            try:
                '''
                    Return the real urls
                '''
                return r.headers['location']
            except:
                pass
        return o_url    

    def transformation(self):
        '''
            Transform redirect o_url's to self.url, delete o_url at 
            the same time
        '''
        self.urls = [self.get_real(o_url) for o_url in self.o_urls]
        del self.o_urls

    def print_urls(self):
        '''
            print self.urls
        '''
        for url in self.urls:
            print(url)

    def print_o_urls(self):
        '''
            printing o_urls
        '''
        for url in self.o_urls:
            print(url)

    def run(self, is_fancy = False):
        assert(not self.is_finish())
        while(not self.is_finish()):
            self.get_html()
            self.get_urls()
            self.transformation()
            if is_fancy: 
                self.print_urls()
            self.switch_url()
            return self.urls

if __name__ == '__main__':
    '''
        Helper information for the baidu crawler
    '''
    help = 'baidu_crawler.py -k <keyword> [-t <timeout> -p <total pages>]'
    keyword = None
    '''
        Time out for crawler's requests
    '''
    timeout  = None
    '''
        Default number of pages is 1 
    '''
    totalpages = 10
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hk:t:p:")
    except getopt.GetoptError:
        print(help)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print(help)
            sys.exit()
        elif opt in ("-k", "--keyword"):
            keyword = arg
        elif opt in ("-t", "--timeout"):
            timeout = arg
        elif opt in ("-p", "--totalpages"):
            totalpages = arg
    '''
        keyword should be non-empty
    '''
    if keyword == None:
        print(help)
        sys.exit()

    _crawler = crawler(keyword)
    if timeout != None:
        _crawler.set_timeout(timeout)
    if totalpages != None:
        _crawler.set_total_pages(totalpages)
    urls = _crawler.run(True)